
import { By } from '@angular/platform-browser';
import { TestBed, async,fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {SearchGiphyComponent } from './search.component';
import { CustomModal } from '../dialogGifs/customModal.component';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import {  OverlayRenderer, DOMOverlayRenderer, Overlay } from 'ngx-modialog';
import { HttpClientModule } from '@angular/common/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


var app;
var fixture;
const MODAL_PROVIDERS = [
  Modal,
  Overlay,
  { provide: OverlayRenderer, useClass: DOMOverlayRenderer }
];
describe('SearchGiphyComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule
      ],
      declarations: [
        SearchGiphyComponent,
        CustomModal
      ],
      providers:[Modal,MODAL_PROVIDERS,Ng4LoadingSpinnerService]

     
    }).compileComponents().then(()=>{
        fixture = TestBed.createComponent(SearchGiphyComponent);
        app = fixture.componentInstance;
       // console.log(app.gifs)
      //   console.log(fixture.debugElement)
    });
  }));

  it('should create the search component', () => {
    expect(app.test).toEqual("hi");
  });

  it('should allow us to set a bound input field', fakeAsync(() => {
    setInputValue('#test2', 'Tommy');

    expect(app.search).toEqual('Tommy');
  }));

  // must be called from within fakeAsync due to use of tick()
  function setInputValue(selector: string, value: string) {
    fixture.detectChanges();
    tick();

    let input = fixture.debugElement.query(By.css(selector)).nativeElement;
    input.value = value;
    input.dispatchEvent(new Event('input'));
    tick();
  }
});
