import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

    link = "http://api.giphy.com/v1/gifs/search?api_key=Nt7086vC7bbGnNchzNYBNRH2WTlYNyG5&q=";

  constructor(private http: HttpClient) { }

  getGiphies(searchTerm) {
      var baseLink = this.link +searchTerm;
    return this.http.get(baseLink)
    .pipe(
                catchError(this.handleError)
    );
  }

  
 handleError(error) {
  let errorMessage = '';
  console.log(error)
  if(error.status == 429){
    window.alert("Network is down. Please try after some time.");
    window.location.reload();
    return;
  }
  
  else {
    // server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
window.location.reload();
  return throwError(errorMessage);
}

}