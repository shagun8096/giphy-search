import { Component, AfterViewInit, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { SearchService } from './search.service';
import { Overlay, overlayConfigFactory } from 'ngx-modialog';
import { Modal, BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import { CustomModalContext, CustomModal } from '../dialogGifs/customModal.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
    selector: "app-search",
    templateUrl: "./search.component.html",
    styleUrls: ["./search.component.css"],
    providers: [Modal],

})

export class SearchGiphyComponent{
gifs =[];
test = "hi"
gifsBoolean = false;
search = "Tommy"
    constructor(private searchService:SearchService,public modal: Modal,private spinner: Ng4LoadingSpinnerService) {
    }

    searchGiphy(searchTerm: HTMLInputElement){
      this.search= searchTerm.value;
            this.spinner.show();
            this.searchService.getGiphies(searchTerm.value).subscribe( res =>
             {
             this.gifs = res["data"];
             this.spinner.hide();
            if(this.gifs.length == 0 && searchTerm.value !== ''){
                this.gifsBoolean = true;
            }
            else{
                this.gifsBoolean = false;
            }
                  });
     
    

    }


    playGif(images){
        return this.modal.open(CustomModal,  overlayConfigFactory({images: images }, BSModalContext));
       
    }

}