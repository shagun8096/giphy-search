import { BrowserModule }    from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchGiphyComponent } from './searchModule/search.component';
import { OverlayModule,OVERLAY_PROVIDERS } from '@angular/cdk/overlay';
import { Modal, BSModalContext } from 'ngx-modialog/plugins/bootstrap';

import { ModalModule, OverlayRenderer, DOMOverlayRenderer, Overlay } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { CustomModal } from './dialogGifs/customModal.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

const MODAL_PROVIDERS = [
  Modal,
  Overlay,
  { provide: OverlayRenderer, useClass: DOMOverlayRenderer }
];

@NgModule({
  declarations: [
    AppComponent,
    SearchGiphyComponent,
    CustomModal
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    Ng4LoadingSpinnerModule ,
    OverlayModule
  ],

  providers: [OVERLAY_PROVIDERS,Modal,MODAL_PROVIDERS],
  entryComponents: [ CustomModal ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
